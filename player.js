var player = {
	'y':20,
	'x':20,
	'height':40,
	'width':40 , 
  
 	bottom: function () {  return player.y + ( player.height / 2 );	},

 	'x_vector':0,
 	'y_vector':0,

 	getX: function () {	player.x  = player.x + player.x_vector; return player.x;},
 	getY: function () {	player.y  = player.y + player.y_vector; return player.y;},
 };
