 


var gravity = 0.1;
var key_down = false;

var level_offset_X = 0;
var level_offset_Y = 0 ;

var canvas_width  = 1000;
var canvas_height = 500;
var acceleration = 2;


var default_color = 'rgb(255,255,255)';
 
var structure = []; 



function setup() {

	createCanvas(canvas_width, canvas_height);
	background(153);

 	for (var i = level.length - 1; i >= 0; i--) {
		structure.push(new Platform(level[i]));
	}

 
 }
    


function draw() {

	clear();

	createCanvas(canvas_width, canvas_height);
    background(153);


    metric('player_x_vector',player.x_vector);
    metric('player_y_vector',player.y_vector);
 
    player.intersect = false;

    for (var i = structure.length - 1; i >= 0; i--) {
    	platform = structure[i];
    	fill(color(platform.color));
    	rect(platform.getX() , platform.getY() , platform.length, platform.height );
    
    	platform.playerIntersect(player);


    }
    metric('player_intersect',player.intersect);	

	console.log(player.y_vector);

    if (player.bottom() >= height) { // player is at bottom of screen
		if (key_down == false) {
			player.y_vector = 0;
		}
	}
	

	if( player.x_vector > 0 && player.x >= width){
		level_offset_X -= player.x_vector;
		player.x_vector = 0;

	}

	// if (player.x >= width) { // player is at bottom of screen
	// 	player.x_vector = 0;
	// }

	if(!player.intersect){	player.y_vector += gravity; } 
	 
	console.log(player.y_vector);


  	ellipse( player.getX() ,player.getY()  , player.height, player.width);
  	key_down = false;
}

function keyPressed() {
 	key_down = true;

    switch(keyCode)
    {
    	case UP_ARROW:
 			player.y_vector =- 5;
    	break;
		
		case LEFT_ARROW:
    		player.x_vector --;  
    	break;
    	
    	case RIGHT_ARROW:
    		player.x_vector ++;      	
    	break;    	

   
    }
 }

 
 
function metric(id,val)
{
	document.getElementById(id).innerHTML   = id + '  ' + val;
}







