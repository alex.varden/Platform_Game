function Platform (level)
{

	this.x = level.x;
	this.y = level.y;
	this.height = level.height;
	this.length = level.length;
	this.color = level.color;


	this.playerIntersect = function()
	{
		this.intersect = false;	


		if(
    	    (player.x >= platform.x &&  					// player after start of platform
    	   	 player.x <= (platform.x + platform.length))  // player before end of platform 
 		){
 
     		if( player.y >= ( platform.y - (platform.height/2) ) &&	  //  bellow top of stucture 
     		    player.y <= ( platform.y + (platform.height/2) ) )    //  but not under it 
     		{
				this.intersect = true;
				player.intersect = true;

				if ( key_down  == false) {
					player.y_vector = 0;
			 		// friction
					if( player.x_vector>0){	  player.x_vector -= 0.06; }
					if( player.x_vector<0){	  player.x_vector += 0.06; } 
 				}

				return
			}
    	}  

	}


	this.getX = function(){ return this.x + level_offset_X	}
	this.getY = function(){ return this.y + level_offset_Y	}
 
}


